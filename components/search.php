<?php
/**
 * Search.php - Handy functions for search. Mainly used in the search.php controller, but might also be handy in some ajax calls.
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';

/**
 * Searces a text in multiple ways and returnes an array of search results.
 *
 * @param $patterns a
 *        	variable-length argument list (@see http://php.net/manual/en/functions.arguments.php#functions.variable-arg-list).
 *        	it represents one or more search patterns to look for
 *        	
 * @return an array containing the article ID and a sample content of each hit for the search pattern(s)
 */
function search_pattern(...$patterns) {
	$ret_val = Array ();
	// Search for individual words in each pattern
	foreach ( $patterns as $pattern ) {
		$words = explode ( ' ', $pattern );
		if (count ( $words ) > 1) {
			foreach ( $words as $word ) {
				// Using recursion here (this function calls itself)
				$ret_val = array_merge ( $ret_val, search_pattern ( $word ) );
			}
		}
	}
	// search for exact match of each pattern
	foreach ( $patterns as $pattern ) {
		$ret_val = array_merge ( $ret_val, do_search ( $pattern ) );
	}
	return $ret_val;
}

/*
 * Helper function te search one specific pattern
 */
function do_search($pattern) {
	global $mysqli;
	$ret_val = Array ();
	if ($pattern == NULL || strlen ( $pattern ) == 0)
		return $ret_val;
	$sql = "SELECT * FROM ARTICLE WHERE Content LIKE '%" . $pattern . "%';";
	$result = $mysqli->query ( $sql );
	if ($result) {
		while ( $row = $result->fetch_assoc () ) {
			$row ['Content'] = prepare_content ( $row ['Content'], $pattern );
			$ret_val [$row ['ID']] = $row;
		}
	}
	return $ret_val;
}

/*
 * Prepares the content to display as a search result
 */
function prepare_content($content, $pattern) {
	$startpos = 25;
	$content = strip_tags ( $content ); // strip all html codes
	$index = stripos ( $content, $pattern ); // find the first
	if ($index) {
		$startpos = $index - 100;
		if ($startpos < 0) {
			$startpos = 0;
		}
		$prefix = substr ( $content, $startpos, $index - $startpos );
		$pattern = substr ( $content, $index, strlen ( $pattern ) );
		$suffix = substr ( $content, $index + strlen ( $pattern ), 100 );
		$result = $prefix . '<b>' . $pattern . '</b>' . $suffix;
		return $result;
	}
	return "error. pattern [$pattern] not found in content [$content]";
}

?>